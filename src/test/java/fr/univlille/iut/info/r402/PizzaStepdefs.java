package fr.univlille.iut.info.r402;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PizzaStepdefs {
    Four four;
    Pizza maPizza;
    Pizzaiolo pizzaiolo;

    @Given("^un four")
    public void unFour() {
        four = new Four();
    }

    @And("^un pizzaiolo")
    public void unPizzaiolo() {
        pizzaiolo = new Pizzaiolo();
    }

    @And("^le pizzaiolo prépare une pizza reine")
    public void preparerReine() {
        maPizza = pizzaiolo.preparer("reine");
    }
    @When("^le pizzaiolo met la pizza reine au four$")
    public void pizzaAuFour() {
        pizzaiolo.mettreAuFour(four, maPizza);
    }

    @Then("au bout de {int} ticks d'horloge, la pizza est cuite")
    public void pizzaCuite(int ticks) {
        assertDoesNotThrow(() ->{
            Thread.sleep(ticks);
            assertTrue(maPizza.estCuite());
        });
    }

    @Then("au bout de {int} ticks d'horloge, la pizza est brulee")
    public void pizzaBrulee(int ticks) {
        assertDoesNotThrow(() ->{
            Thread.sleep(ticks);
            assertTrue(maPizza.estBrulee());
        });
    }
}

