package fr.univlille.iut.info.r402;

public class Pizza {
    private int prix;
    private String libelle;
    private long tickMiseAuFour;

    private static final int PRIX_DE_BASE = 8;

    public Pizza(String libelle) {
        this.prix = PRIX_DE_BASE;
        this.libelle = libelle;
        this.tickMiseAuFour = -1L;
    }

    public boolean estCuite() {
        return this.tickMiseAuFour != -1L &&
        (System.currentTimeMillis() - this.tickMiseAuFour) >= 30 && 
        (System.currentTimeMillis() - this.tickMiseAuFour) < 60;
    }

    public boolean estBrulee(){
        return this.tickMiseAuFour != -1L &&
        (System.currentTimeMillis() - this.tickMiseAuFour) >= 60;
    }

    public void definirTickMiseAuFour(long tickMiseAuFour) {
        this.tickMiseAuFour = tickMiseAuFour;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + prix;
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pizza other = (Pizza) obj;
        if (prix != other.prix)
            return false;
        if (libelle == null) {
            if (other.libelle != null)
                return false;
        } else if (!libelle.equals(other.libelle))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pizza " + this.libelle + " : " + this.prix;
    }

    public Object getLibelle() {
        return this.libelle;
    }

    
}
