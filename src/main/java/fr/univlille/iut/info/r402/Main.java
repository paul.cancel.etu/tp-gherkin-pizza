package fr.univlille.iut.info.r402;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Pizza pizza = null;
        Pizzaiolo player = new Pizzaiolo();
        Four four = new Four();
        String line = "";
        System.out.println("Jeu des pizzas\n\n");


        try(Scanner sc = new Scanner(System.in)){
            while(!line.equals("quitter")){
                System.out.print("Player : ");
                line = sc.nextLine();
                String[] tab = line.split(" ");

                switch (tab[0]) {
                    case "preparer":
                        if(tab.length>2 && tab[1].equals("pizza")){
                            pizza = player.preparer(tab[2]);
                            System.out.println("Préparation de la pizza "+ tab[2]+"\n");
                        }
                        break;
                    case "cuire":
                        if(tab.length>2 && tab[1].equals("pizza") && pizza != null){
                            player.mettreAuFour(four, pizza);
                            System.out.println("Mise en cuisson de la pizza "+ tab[2]+"\n");
                            pizza = null;
                        }

                        break;
                    case "voir":
                        if(tab.length>1 && tab[1].equals("four")){
                            System.out.println(four.toString());
                        }

                    case "jeter":
                        if(tab.length>2 && tab[1].equals("pizza")){
                           four.jeter(tab[2]);
                           System.out.println("Mise a la poubelle de la pizza "+ tab[2]+"\n");
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
