package fr.univlille.iut.info.r402;

import java.util.ArrayList;
import java.util.List;

public class Four {
    private static final int CAPACITE_DE_BASE = 3;

    private int capacite;
    private List<Pizza> enCuisson;

    public Four(int capacite){
        this.capacite = capacite;
        this.enCuisson = new ArrayList<>();
    }

    public Four(){
        this(CAPACITE_DE_BASE);
    }

    public void ajouter(Pizza maPizza) {
        if(this.enCuisson.size() <= this.capacite){
            this.enCuisson.add(maPizza);
            maPizza.definirTickMiseAuFour(System.currentTimeMillis());
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Pizza p : this.enCuisson){
            sb.append(p.toString() + "\n");
        }
        return sb.toString();
    }

    public List<Pizza> getEnCuisson() {
        return this.enCuisson;
    }

    public void jeter(String pizza) {
        Pizza res = null;
        for(Pizza p : this.enCuisson){
            if(p.getLibelle().equals(pizza)){
                res = p;
            }
        }
        this.enCuisson.remove(res);
    }
}
