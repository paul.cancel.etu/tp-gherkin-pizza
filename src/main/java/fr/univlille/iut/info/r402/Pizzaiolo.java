package fr.univlille.iut.info.r402;

public class Pizzaiolo {
    private static final int ARGENT_DEBUT = 100;

    private int argentTotal;



    public Pizza preparer(String libelle) {
        return new Pizza(libelle);
    }

    public void mettreAuFour(Four four, Pizza maPizza) {
        four.ajouter(maPizza);
    }
}
